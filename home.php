<?php 
//Template Name: Lista Lobos
?>
<?php get_header(); ?>
    <main>
        <section class="search-adopted">
            <div class="search">
                <form action="" class="search-form">
                    <input class="search-button" type="button" value="search" onclick="filtraLobos()">
                    <input class="search-name" type="text">
                </form>
                <input class="add-lobo" type="button" value="+ Lobo">
            </div>
            <div class="adopted">
                <input id="lobos-adotados" type="checkbox">
                <label for="lobos-adotados">Ver lobinhos adotados</label>
            </div>
        </section>
        <section class="lista-lobos">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="perfil-lobo">
                <img class="img-lobo" src="<?php echo the_field('imagem')?>" alt="uma foto do(a) lobo(a) ${items[i].name}">
                <div class="texto-lobo">
                    <div class="header-lobo">
                        <div class="nome-idade">
                            <h2><?php the_field('nome') ?></h2>
                            <p>Idade: <?php the_field('idade') ?> anos</p>
                        </div>
                        <input class="adotar-btn" type="button" value="Adotar" onclick="window.location.href = '<?php echo get_permalink(); ?>'">
                    </div>
                    <div class="descricao-lobo">
                        <p><?php the_field('descricao') ?></p>
                    </div>
                </div>
            </div>
        <?php 
            endwhile; 
            my_pagination(); 
            else: 
        ?>
            <p>desculpe, o post não segue os critérios escolhidos</p>
        <?php endif; ?>
        </section>
        
    </main>
    <?php get_footer();?>