<?php
//Template Name: Cadastro Lobo
?>
<?php get_header(); ?>
        <main class="container-submit">
            
            <div class="container-form">
                <h1>Coloque um Lobinho para Adoção</h1>
                <form class="adopt-form">
                   <div class="container-name-n-age">
                       <div class="label-above">
                            <label for="nome">Nome do Lobinho</label>
                            <input type="text" id="nome" name="name"/>
                        </div>
                        <div class="label-above">
                            <label for="idade">Anos</label>
                            <input type="text" id="idade" name="age"/>
                        </div>
                        
                    </div>
                    <div class="label-above">
                        <label for="foto">Link da Foto</label>
                        <input type="text" id="foto" name="imgLink"/>
                    </div>
                    <div class="label-above">
                        <label for="desc">Descrição</label>
                    <input type="text" id="desc" name="description"/>
                    </div>
                    
                    <input type="submit" id="submit" value="Enviar">
                </form>
            </div>           
        </main>
        <?php get_footer();?>