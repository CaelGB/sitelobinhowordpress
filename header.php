<!DOCTYPE html>
<html>
    <head>
        <title>Adote um lobinho</title>
        <meta name="Adote um lobinho" content="Pagina de adoção de lobinhos" charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Darker+Grotesque:wght@500&display=swap" rel="stylesheet"> 
        
        <link rel="stylesheet" href="style/reset.css">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri()?>/style.css">
        <?php wp_head();?>
    </head>
    <body>
        <header class="header">
            <a href ="http://adote-um-lobinho.local/lista-lobos/" class="header-link">Nossos Lobinhos</a>
            <a href ="http://adote-um-lobinho.local/home/" class="header-link">
                <img src="<?php echo get_stylesheet_directory_uri()?>/img/logoLobinhos.png" class="logo" alt="Logo 'Adote um lobinho'"/>
            </a>
            <div style="display: flex; flex-flow: column;">
                <a href ="http://adote-um-lobinho.local/quem-somos/" class="header-link">Quem Somos</a>
                <div class ="black-rectangle"></div>
            </div> 
        </header>