<?php
//Template Name: Adotar Lobo
?>
<?php get_header(); ?>
        <main class="adopt-screen">
            <div class="wolf">
                <img src="img/placeholderWolf.png" alt="Imagem de um lobo" class="wolf-img">
                <div class="wolf-info">
                    <h1 class="wolf-name">Adote o(a) NomeDoLobo</h1>
                    <p class="wolf-id">ID:312159</p>
                </div>
            </div>
            <form class="adopter-info">
                <div class="personal-info">
                    <div class="label-on-top name-container">
                        <label for="name">Seu nome:</label>
                        <input type="text" id="name" name="name"/>
                    </div>
                    <div class="label-on-top age-container">
                        <label for="age">Idade:</label>
                        <input type="text" id="age" name="age"/>
                    </div>
                </div>
                <div class="label-on-top email-container">
                    <label for="email">E-mail:</label>
                    <input type="text" id="email" name="email"/>
                </div>
                <input type="submit" value="Adotar" class="button-adopt"/>
            </form>        
        </main>
<?php get_footer();?>