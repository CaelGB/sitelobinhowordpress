<?php
//Template Name: Show Lobinho
?>
<?php get_header(); ?>
        <main class="main">
        
        <div class="perfil-lobo">
                <h1><?php echo the_field('nome')?></h1>
                <div class="img-descricao">
                        <div class="img-botoes">
                                <img class="img-lobo" src="<?php the_field('imagem')?>alt="uma foto do(a) lobo(a)">
                        </div>
                        <div class="descricao-lobo">
                                <p><?php the_field('descricao') ?></p>
                        </div>
                </div>
        </div>

        </main>
<?php get_footer();?>