<?php

function wpdocs_theme_name_styles() {
    wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css' );
    if ( is_page('quem-somos') ){
      wp_enqueue_style( 'quemSomos', get_template_directory_uri() . '/style/quemSomos.css' );
    } 
    if ( is_home()){
        wp_enqueue_style( 'listaDeLobinhos', get_template_directory_uri() . '/style/listaDeLobinhos.css' ); 
    }
    if (is_single()){
        wp_enqueue_style('showLobinho', get_template_directory_uri() . '/style/showLobinho.css');
    }
    if (is_page('Adotar Lobo')){
        wp_enqueue_style('styleAdotar', get_template_directory_uri() . '/style/styleAdotar.css');
    }
    if (is_page('Cadastro Lobo')){
        wp_enqueue_style('styleCadastro', get_template_directory_uri() . '/style/styleCadastro.css');
    }
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_styles' );

add_theme_support( 'widgets' );
add_theme_support( 'menus' );

function my_pagination() {
    global $wp_query;
    
        echo paginate_links( array(
        'base' => str_replace( 9999999999999, '%#%', esc_url( get_pagenum_link( 9999999999999 ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total' => $wp_query->max_num_pages,
        'type' => 'list',
        'prev_next' => true,
        'prev_text' => 'Página Anterior',
        'next_text' => 'Próxima Página',
        'before_page_number' => '-',
        'after_page_number' => '>',
        'show_all' => false,
        'mid_size' => 3,
        'end_size' => 1,
        )); 
}
?>